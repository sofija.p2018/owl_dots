## HYPRLAND CATPPUCCIN
<br />
  
<div align="center">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_catppuccin/.img/1.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_catppuccin/.img/2.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_catppuccin/.img/3.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/hyprland/hypr_catppuccin/.img/4.jpg?ref_type=heads" width="550">
</div>
<br /><br />

## ИНФО  

|DISTRO|[EndeavourOS](https://endeavouros.com/)|
| ------ | ------ |
|WM|[Hyprland](https://hyprland.org/)|
|BAR|[Waybar](https://github.com/Alexays/Waybar)|
|LAUNCHER|[Rofi from adi1090x](https://github.com/adi1090x/rofi)|
|TERMINAL|[Foot](https://codeberg.org/dnkl/foot)|
|SHELL|[Fish](https://fishshell.com/)|
|DOCK|[Nwg-dock](https://github.com/nwg-piotr/nwg-dock)|
|NOTIFICATION|[Mako](https://github.com/emersion/mako)|
|LOCKING|[Hyprlock](https://github.com/hyprwm/hyprlock)|  
  
## HI!
Этот Hyprland я ставил на EndeavourOS Linux, при установки убрал галочки со всех окружений,
а Hyprland и софт ставил руками. Конфиги софта лежат по стандартным путям, ничего
сверхестественного.  

Что бы сделать у себя так нужно установить hyprland waybar rofi foot fish nwg-dock mako hyprlock
скачать конфиги и положить в ~/.config и ребутнутся.
    
## УСТАНОВКА ТЕМ, ИКОНОК И ОТКЛЮЧЕНИЕ КНОПОК
```
gsettings set org.gnome.desktop.interface icon-theme Catppuccin-Mocha-Alt2
gsettings set org.gnome.desktop.interface gtk-theme gtk-theme Catppuccin-Mocha-Standard-Lavender-Dark
hyprctl setcursor Catppuccin-Mocha-Rosewater-Cursors 24
gsettings set org.gnome.desktop.interface font-name 'JetBrainsMono 12'

gsettings set org.gnome.desktop.wm.preferences button-layout :

Включение гтк тем и иконок в этом Hyprland прописанна в конфиге самого Hyprland.

Тема иконок собрана из трех, основа Catppuccin-Mocha-Alt2, иконки приложений Tela-Circle,
а иконки урпавления breeze.

```

|ICON|[Catppuccin-Mocha-Alt2](https://www.gnome-look.org/p/1715570)|
| ------ | ------ |
|GTK3|[Catppuccin-Mocha-Standard-Lavender-Dark](https://www.pling.com/p/1715554/)|
|CURSORS|[Catppuccin-Mocha-Rosewater-Cursors](https://www.pling.com/p/2135236)|
|FONT|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|SOFT|[Catppuccin](https://github.com/catppuccin/catppuccin?tab=readme-ov-file)|
|WALL_1|[Тут](https://sun9-42.userapi.com/impg/GNfdZPRJVdSN_Q2yfW-g9Udf0BtfyOBeuCDZRA/Rwf1tsDXJtI.jpg?size=1920x1080&quality=95&sign=5f3e1e322c543634c22a71d526e9aad2&type=album)|
  
## НАСТРОЙКА СИСТЕМЫ
  
- [```Установка VoidLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-uefi-install)  
- [```Установка ArchLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/ArchLinux-uefi-install)  
- [```Автостарт и Автологин```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Autostart_wm)  
- [```Установка Apparmor```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Apparmor)  
- [```Установка Lutris```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Lutris)  
- [```Установка Virt-manager```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Virt-Manager)  
- [```VoidLinux Wayland WM```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-Wayland-WM)   
- [```Установка Pipewire```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Pipewire)   
  
## ВИДЕО
[![Watch the video](https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/hypr_catppuccin.jpg?ref_type=heads)](https://www.youtube.com/watch?v=xTkb1bCr1zs)

## ССЫЛКИ
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_youtube.png?ref_type=heads" width="100">](https://www.youtube.com/@prolinux2753)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_tg.png?ref_type=heads" width="100">](https://t.me/prolinux_tg)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_unsplash.png?ref_type=heads" width="100">](https://unsplash.com/@owl410/collections)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_vk.png?ref_type=heads" width="100">](https://vk.com/prolinux410)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_dzen.png?ref_type=heads" width="100">](https://dzen.ru/prolinux410)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_coffee.png?ref_type=heads" width="100">](https://www.donationalerts.com/r/prolinux)

