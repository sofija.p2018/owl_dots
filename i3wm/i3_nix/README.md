## I3 NixOS
<br />
  
<div align="center">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/i3wm/i3_nix/.img/1.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/i3wm/i3_nix/.img/2.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/i3wm/i3_nix/.img/3.jpg?ref_type=heads" width="550">
<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/i3wm/i3_nix/.img/4.jpg?ref_type=heads" width="550">
</div>
<br /><br />

## ИНФО  
|DISTRO|[NixOS](https://nixos.org/)|
| ------ | ------ |
|WM|[i3wm](https://i3wm.org/)|
|BAR|[Polybar](https://github.com/polybar/polybar)|
|LAUNCHER|[rofi](https://github.com/davatorium/rofi)|
|TERMINAL|[Alacritty](https://github.com/alacritty/alacritty)|


## HI!
Конфиг собирался на NixOS но обсолютно имеративно, то есть для того что бы притащить это все в другой
дистрибутив достаточно установить софт, склонировать репу git'ом и закинуть все в ~/.config.
Ну и установить темы и иконки через например lxappearance  
  
    
## СОФТ
```
telegram-desktop firefox alacritty scrot cmus cava htop git fastfetch
nemo file-roller mpv viewnior polybar jq rofi nitrogen lxappearance conky picom
```

|ICON|[Delight](https://www.pling.com/p/1532276)|
| ------ | ------ |
|GTK3|[Flat-Remix-GTK-Gray](https://github.com/daniruiz/flat-remix-gtk)|
|CURSORS|[Capitaine-cursors](https://github.com/keeferrourke/capitaine-cursors)|
|FONT|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|WALL|[Тут](https://sun9-24.userapi.com/impg/DXnMo9iqaciT08eKECDFVWGI5H2ygXPO7ZZSug/gGu42IcFbDM.jpg?size=1456x816&quality=95&sign=dbc7946011cc0d33ca2dd623d5585e2f&type=album)|
  
## НАСТРОЙКА СИСТЕМЫ
  
- [```Установка VoidLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-uefi-install)  
- [```Установка ArchLinux```](https://gitlab.com/prolinux410/owl_dots/-/wikis/ArchLinux-uefi-install)  
- [```Автостарт и Автологин```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Autostart_wm)  
- [```Установка Apparmor```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Apparmor)  
- [```Установка Lutris```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Lutris)  
- [```Установка Virt-manager```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Virt-Manager)  
- [```VoidLinux Wayland WM```](https://gitlab.com/prolinux410/owl_dots/-/wikis/VoidLinux-Wayland-WM)   
- [```Установка Pipewire```](https://gitlab.com/prolinux410/owl_dots/-/wikis/Pipewire)   
  
## ССЫЛКИ
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_youtube.png?ref_type=heads" width="100">](https://www.youtube.com/@prolinux2753)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_tg.png?ref_type=heads" width="100">](https://t.me/prolinux_tg)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_unsplash.png?ref_type=heads" width="100">](https://unsplash.com/@owl410/collections)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_vk.png?ref_type=heads" width="100">](https://vk.com/prolinux410)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_dzen.png?ref_type=heads" width="100">](https://dzen.ru/prolinux410)
[<img src="https://gitlab.com/prolinux410/owl_dots/-/raw/main/.img/git_coffee.png?ref_type=heads" width="100">](https://www.donationalerts.com/r/prolinux)

